//FOOTER START

kleiner = false;

function verkleinern() {
  if (kleiner == false) {
    document.querySelectorAll('.footer')[0].style.height = '50px';
    document.querySelectorAll('.footer')[0].style.boxShadow = 'none';
    document.querySelectorAll('.footer')[0].style.background = 'none';
    document.getElementById('pfeil').innerHTML = '&#10555;';
    document.getElementById('pfeil').style.background = '#191919';
    document.getElementById('pfeil').style.margin = '5px';
    document.getElementById('pfeil').style.border = '2px solid white';
    document.getElementById('copyright').style.opacity = '0';
    kleiner = true;
  }
  else {
    document.getElementById('copyright').style.opacity = '1';
    document.querySelectorAll('.footer')[0].style.boxShadow = '1px 1px 10px #f3e500';
    document.getElementById('pfeil').style.border = '2px solid white';
    document.querySelectorAll('.footer')[0].style.height = 'auto';
    document.querySelectorAll('.footer')[0].style.background = '#191919';
    document.getElementById('pfeil').innerHTML = '&#8631;';
    kleiner = false;
  }
}

//FOOTER END

function motivation_weiterleiten(){
  window.open("motivation.html", "_self");
}

function uebungen_weiterleiten(){
  window.open("uebungen.html", "_self");
}

function get2home(){
  window.open("index.html", "_self");
}

function Anreise(){
  window.open("Anreise.html", "_self");
}

function Historie(){
  window.open("Historie.html", "_self");
}

function Partner(){
  window.open("Entwurf(Design).html", "_self");
}

function Philosophie(){
  window.open("Philosophie.html", "_self");
}

function Reparaturen(){
  window.open("Reparaturen.html", "_self");
}

function Tuning(){
  window.open("Tuning.html", "_self");
}

function Teile(){
  window.open("Teile.html", "_self");
}

function Felgen(){
  window.open("https://www.premio.de/felgenkonfigurator#272130762137C5FB8", "_blank");
}

function get2home2(){
  window.open("../index.html", "_self");
}

//Partner-Links Start

function meguiars(){
  window.open("https://www.meguiars.de/", "_blank");
}

function BBS(){
  window.open("https://bbs.com/de/", "_blank");
}

function KW(){
  window.open("https://www.kwsuspensions.de/", "_blank");
}

function OZ(){
  window.open("https://www.ozracing.com/de/", "_blank");
}

function Bastuck(){
  window.open("https://www.bastuck.de/de/", "_blank");
}

function HR(){
  window.open("https://www.h-r.com/", "_blank");
}

function download(){
  window.open("https://www.knluftfilter.com/", "_blank");
}

function LSD(){
  window.open("https://www.lsdshop.de/", "_blank");
}

function OXigin(){
  window.open("https://www.oxigin.de/", "_blank");
}

function logo(){
  window.open("https://spurverbreiterung.de/de/", "_blank");
}

function ST(){
  window.open("https://www.st-suspensions.de/shop", "_blank");
}

function eisenmann(){
  window.open("https://eisenmann-exhaust-systems.de/", "_blank");
}

function CK(){
  window.open("https://www.ck-cabrio.de/", "_blank");
}

function OIP(){
  window.open("https://www.tuvsud.com/de-de/standorte", "_blank");
}

//Partner-Links Ende

//SOCIAL-BAR START
function facebook(){
  window.open("https://www.facebook.com/");
}

function instagram(){
  window.open("https://www.instagram.com/");
}

function youtube(){
  window.open("https://www.youtube.com/");
}

function twitter(){
  window.open("https://twitter.com/home");
}
//SOCIAL-BAR END

//PLAYLISTS START
function playlist1(){
  window.open("https://open.spotify.com/playlist/3ybnF1ZkG9Y2LZhr15eFQi?si=p7b79EzyTE-TEVqIu5m4WQ");
}

function playlist2(){
  window.open("https://open.spotify.com/playlist/37i9dQZF1DX76Wlfdnj7AP?si=IVL1aDfaTZ-PDecn8iumfQ");
}

function playlist3(){
  window.open("https://open.spotify.com/playlist/37i9dQZF1DXdxcBWuJkbcy?si=AJ4wN6IxQASXTv2hz3ADlg");
}
//PLAYLISTS END

//HOME-SHOPS START
function zecplus_(){
  window.open("https://www.zecplus.de/");
}

function smilodox_(){
  window.open("https://smilodox.com/");
}

function gymshark_(){
  window.open("https://de.gymshark.com/");
}
//HOME-SHOPS END

//EXERCISES START
function traps(){
  window.open("Übungen/nacken.html");
}

function shoulders(){
  window.open("Übungen/schultern.html");
}

function brust(){
  window.open("Übungen/brust.html");
}

function bizeps(){
  window.open("Übungen/bizeps.html");
}

function unterarme(){
  window.open("Übungen/unterarme.html");
}

function sixpack(){
  window.open("Übungen/bauchmuskeln.html");
}

function oberschenkel(){
  window.open("Übungen/oberschenkel.html");
}

function waden(){
  window.open("Übungen/waden.html");
}

function back_traps(){
  window.open("Übungen/trapezius.html");
}

function trizeps(){
  window.open("Übungen/trizeps.html");
}

function latissimus(){
  window.open("Übungen/latissimus.html");
}

function untererRuecken(){
  window.open("Übungen/unterer_ruecken.html");
}

function gesaess(){
  window.open("Übungen/gesaess.html");
}

//EXERCISES END

bild1_angeklickt = false;
bild2_angeklickt = false;
bild3_angeklickt = false;
bild4_angeklickt = false;
bild5_angeklickt = false;
bild6_angeklickt = false;
anmelden_angeklickt = false;
registrieren_angeklickt = false;

function bild1_click(){
  if (bild1_angeklickt == false) {
    document.getElementById("bild1").src="../Bilder/Motivation/Bild1_Spruch.png";
    bild1_angeklickt = true;
  }
  else {
    document.getElementById("bild1").src="../Bilder/Motivation/Bild1und6.png";
    bild1_angeklickt = false;
  }
}

function bild2_click(){
  if (bild2_angeklickt == false) {
    document.getElementById("bild2").src="../Bilder/Motivation/Bild2_Spruch.png";
    bild2_angeklickt = true;
  }
  else {
    document.getElementById("bild2").src="../Bilder/Motivation/Bild2und5.png";
    bild2_angeklickt = false;
  }
}

function bild3_click(){
  if (bild3_angeklickt == false) {
    document.getElementById("bild3").src="../Bilder/Motivation/Bild3_Spruch.png";
    bild3_angeklickt = true;
  }
  else {
    document.getElementById("bild3").src="../Bilder/Motivation/Bild3.png";
    bild3_angeklickt = false;
  }
}

function bild4_click(){
  if (bild4_angeklickt == false) {
    document.getElementById("bild4").src="../Bilder/Motivation/Bild4_Spruch.png";
    bild4_angeklickt = true;
  }
  else {
    document.getElementById("bild4").src="../Bilder/Motivation/Bild4.png";
    bild4_angeklickt = false;
  }
}

function bild5_click(){
  if (bild5_angeklickt == false) {
    document.getElementById("bild5").src="../Bilder/Motivation/Bild5_Spruch.png";
    bild5_angeklickt = true;
  }
  else {
    document.getElementById("bild5").src="../Bilder/Motivation/Bild2und5.png";
    bild5_angeklickt = false;
  }
}

function bild6_click(){
  if (bild6_angeklickt == false) {
    document.getElementById("bild6").src="../Bilder/Motivation/Bild6_Spruch.png";
    bild6_angeklickt = true;
  }
  else {
    document.getElementById("bild6").src="../Bilder/Motivation/Bild1und6.png";
    bild6_angeklickt = false;
  }
}

function weiterleitung_anmelden() {
  if (anmelden_angeklickt == false){
    document.getElementById("anmelden_funk").innerHTML = "coming soon...";
    anmelden_angeklickt = true;
  }
  else {
    document.getElementById("anmelden_funk").innerHTML = "Anmelden";
    anmelden_angeklickt = false;
  }
}

function weiterleitung_registrieren() {
  if (registrieren_angeklickt == false){
    document.getElementById("registrieren_funk").innerHTML = "coming soon...";
    registrieren_angeklickt = true;
  }
  else {
    document.getElementById("registrieren_funk").innerHTML = "Registrieren";
    registrieren_angeklickt = false;
  }
}

document.addEventListener('animationstart', function (e) {
  if (e.animationName === 'fade-in') {
      e.target.classList.add('did-fade-in');
  }
});

document.addEventListener('animationend', function (e) {
  if (e.animationName === 'fade-out') {
      e.target.classList.remove('did-fade-in');
   }
});

//SLIDESHOW
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}
