function speichern() {
  var name_koerperbereich = document.getElementById("koerperbereich").value;
  var name_koerperbereich2 = document.getElementById("koerperbereich2").value;
  var name_koerperbereich3 = document.getElementById("koerperbereich3").value;
  var name_koerperbereich4 = document.getElementById("koerperbereich4").value;
  var wiederholungen1 = document.getElementById("wiederholung1").value;
  var wiederholungen2 = document.getElementById("wiederholung2").value;
  var wiederholungen3 = document.getElementById("wiederholung3").value;
  var wiederholungen4 = document.getElementById("wiederholung4").value;
  var wiederholungen5 = document.getElementById("wiederholung5").value;
  var wiederholungen6 = document.getElementById("wiederholung6").value;
  var wiederholungen7 = document.getElementById("wiederholung7").value;
  var wiederholungen8 = document.getElementById("wiederholung8").value;
  var wiederholungen9 = document.getElementById("wiederholung9").value;
  var wiederholungen10 = document.getElementById("wiederholung10").value;
  var wiederholungen11 = document.getElementById("wiederholung11").value;
  var wiederholungen12 = document.getElementById("wiederholung12").value;
  localStorage.setItem("name_koerper", name_koerperbereich);
  localStorage.setItem("name_koerper2", name_koerperbereich2);
  localStorage.setItem("name_koerper3", name_koerperbereich3);
  localStorage.setItem("name_koerper4", name_koerperbereich4);
  localStorage.setItem("anzahl_wiederholung1", wiederholungen1);
  localStorage.setItem("anzahl_wiederholung2", wiederholungen2);
  localStorage.setItem("anzahl_wiederholung3", wiederholungen3);
  localStorage.setItem("anzahl_wiederholung4", wiederholungen4);
  localStorage.setItem("anzahl_wiederholung5", wiederholungen5);
  localStorage.setItem("anzahl_wiederholung6", wiederholungen6);
  localStorage.setItem("anzahl_wiederholung7", wiederholungen7);
  localStorage.setItem("anzahl_wiederholung8", wiederholungen8);
  localStorage.setItem("anzahl_wiederholung9", wiederholungen9);
  localStorage.setItem("anzahl_wiederholung10", wiederholungen10);
  localStorage.setItem("anzahl_wiederholung11", wiederholungen11);
  localStorage.setItem("anzahl_wiederholung12", wiederholungen12);
}

function lesen() {
  document.querySelector("#koerperbereich").value = localStorage.getItem('name_koerper');
  document.querySelector("#koerperbereich2").value = localStorage.getItem('name_koerper2');
  document.querySelector("#koerperbereich3").value = localStorage.getItem('name_koerper3');
  document.querySelector("#koerperbereich4").value = localStorage.getItem('name_koerper4');
  document.querySelector("#wiederholung1").value = localStorage.getItem('anzahl_wiederholung1');
  document.querySelector("#wiederholung2").value = localStorage.getItem('anzahl_wiederholung2');
  document.querySelector("#wiederholung3").value = localStorage.getItem('anzahl_wiederholung3');
  document.querySelector("#wiederholung4").value = localStorage.getItem('anzahl_wiederholung4');
  document.querySelector("#wiederholung5").value = localStorage.getItem('anzahl_wiederholung5');
  document.querySelector("#wiederholung6").value = localStorage.getItem('anzahl_wiederholung6');
  document.querySelector("#wiederholung7").value = localStorage.getItem('anzahl_wiederholung7');
  document.querySelector("#wiederholung8").value = localStorage.getItem('anzahl_wiederholung8');
  document.querySelector("#wiederholung9").value = localStorage.getItem('anzahl_wiederholung9');
  document.querySelector("#wiederholung10").value = localStorage.getItem('anzahl_wiederholung10');
  document.querySelector("#wiederholung11").value = localStorage.getItem('anzahl_wiederholung11');
  document.querySelector("#wiederholung12").value = localStorage.getItem('anzahl_wiederholung12');
  document.querySelector("#uebung1").value = localStorage.getItem('titel_unterarmplanke');
  document.querySelector("#uebung2").value = localStorage.getItem('titel_dips');
  document.querySelector("#uebung3").value = localStorage.getItem('titel_kniebeugen');
  document.querySelector("#uebung4").value = localStorage.getItem('titel_klimmzuege');
  document.querySelector("#uebung5").value = localStorage.getItem('titel_erhoehte_hechtpresse');
  document.querySelector("#uebung6").value = localStorage.getItem('titel_ausfallschritte_rueckwaerts');
  document.querySelector("#uebung7").value = localStorage.getItem('titel_bench_dips');
  document.querySelector("#uebung8").value = localStorage.getItem('titel_good_mornings');
  document.querySelector("#uebung9").value = localStorage.getItem('titel_wadenheben');
}

function unterarmplanke_day1() {
  console.log("gehe rein");
  var name_unterarmplanke = document.getElementById("uebung_titel1").innerHTML;
  localStorage.setItem("titel_unterarmplanke", name_unterarmplanke);
}

function dips_day1() {
  var name_dips = document.getElementById("uebung_titel7").innerHTML;
  localStorage.setItem("titel_dips", name_dips);
}

function kniebeugen_day1() {
  var name_kniebeugen = document.getElementById("uebung_titel10").innerHTML;
  localStorage.setItem("titel_kniebeugen", name_kniebeugen);
}

function klimmzuege_day1() {
  var name_klimmzuege = document.getElementById("uebung_titel13").innerHTML;
  localStorage.setItem("titel_klimmzuege", name_klimmzuege);
}

function erhoehte_hechtpresse_day1() {
  var name_erhoehte_hechtpresse = document.getElementById("uebung_titel15").innerHTML;
  localStorage.setItem("titel_erhoehte_hechtpresse", name_erhoehte_hechtpresse);
}

function ausfallschritte_rueckwaerts_day1() {
  var name_ausfallschritte_rueckwaerts = document.getElementById("uebung_titel17").innerHTML;
  localStorage.setItem("titel_ausfallschritte_rueckwaerts", name_ausfallschritte_rueckwaerts);
}

function bench_dips_day1() {
  var name_bench_dips = document.getElementById("uebung_titel20").innerHTML;
  localStorage.setItem("titel_bench_dips", name_bench_dips);
}

function good_mornings_day1() {
  var name_good_mornings = document.getElementById("uebung_titel27").innerHTML;
  localStorage.setItem("titel_good_mornings", name_good_mornings);
}

function wadenheben_day1() {
  var name_wadenheben = document.getElementById("uebung_titel29").innerHTML;
  localStorage.setItem("titel_wadenheben", name_wadenheben);
}
